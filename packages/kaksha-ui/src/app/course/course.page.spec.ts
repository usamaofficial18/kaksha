import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursePage } from './course.page';
import { RouterTestingModule } from '@angular/router/testing';
import { CourseService } from './course.service';
import { of, BehaviorSubject } from 'rxjs';
import { ModalController } from '@ionic/angular';

describe('CoursePage', () => {
  let component: CoursePage;
  let fixture: ComponentFixture<CoursePage>;
  const selectedCourse = new BehaviorSubject<string>('');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [CoursePage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: CourseService,
          useValue: {
            getCourse: (...args) => of([{ topics: [] }]),
            selectedCourse,
            getCourseList: (...args) => of([{}]),
          },
        },
        {
          provide: ModalController,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
