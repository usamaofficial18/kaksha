import { Injectable } from '@angular/core';
import { Course } from '../common/interfaces/course.interface';
import { from, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  AUTHORIZATION,
  BEARER_TOKEN_PREFIX,
  ACCESS_TOKEN,
} from '../constants/storage';
import { StorageService } from '../api/storage/storage.service';
import { switchMap } from 'rxjs/operators';
import {
  COURSELIST_ENDPOINT,
  COURSE_ENDPOINT,
  TOPIC_STATUS_ENDPOINT,
  ADD_TOPIC_ENDPOINT,
} from '../constants/url-strings';

@Injectable({
  providedIn: 'root',
})
export class CourseService {
  selectedCourse = new BehaviorSubject<{
    program: string;
    course_name: string;
  }>({
    program: '',
    course_name: '',
  });
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) {}

  getCourse(program_name: string, course_name: string) {
    const params = new HttpParams()
      .set('program_name', program_name)
      .set('course_name', course_name);

    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      switchMap(token => {
        const headers = {
          [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
        };
        return this.http.get<Course[]>(COURSE_ENDPOINT, {
          params,
          headers,
        });
      }),
    );
  }

  getCourseList() {
    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      switchMap(token => {
        const headers = {
          [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
        };
        return this.http.get<Course[]>(COURSELIST_ENDPOINT, {
          headers,
        });
      }),
    );
  }

  changeTopicStatus(program_name, course_name, topic_name: any) {
    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      switchMap(token => {
        const headers = {
          [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
        };
        return this.http.post(
          TOPIC_STATUS_ENDPOINT,
          { program_name, course_name, topic_name },
          {
            headers,
          },
        );
      }),
    );
  }

  addTopic(topic_name: string, program_name: string, course_name) {
    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      switchMap(token => {
        const headers = {
          [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
        };
        return this.http.post(
          ADD_TOPIC_ENDPOINT,
          { program_name, course_name, topic_name },
          {
            headers,
          },
        );
      }),
    );
  }
}
