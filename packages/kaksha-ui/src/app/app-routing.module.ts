import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then(m => m.HomePageModule),
  },
  {
    path: 'callback',
    loadChildren: () =>
      import('./callback/callback.module').then(m => m.CallbackPageModule),
  },
  {
    path: 'callback',
    loadChildren: './callback/callback.module#CallbackPageModule',
  },

  { path: 'course', loadChildren: './course/course.module#CoursePageModule' },
  {
    path: 'course/:program/:course',
    loadChildren: './course/course.module#CoursePageModule',
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'add-course',
    loadChildren: './add-course/add-course.module#AddCoursePageModule',
  },
  {
    path: 'attendance',
    loadChildren: () =>
      import('./attendance/attendance.module').then(
        m => m.AttendancePageModule,
      ),
  },
  {
    path: 'important-note',
    loadChildren: () =>
      import('./important-note/important-note.module').then(
        m => m.ImportantNotePageModule,
      ),
  },
  {
    path: 'quiz',
    loadChildren: () =>
      import('./quiz/quiz.module').then(m => m.QuizPageModule),
  },
  {
    path: 'file',
    loadChildren: () =>
      import('./file/file.module').then(m => m.FilePageModule),
  },

  {
    path: 'add-draft',
    loadChildren: () =>
      import('./add-draft/add-draft.module').then(m => m.AddDraftPageModule),
  },

  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
