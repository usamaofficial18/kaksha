import { Component, OnInit } from '@angular/core';
import { Course } from '../common/interfaces/course.interface';
import { FormControl } from '@angular/forms';
import {
  Attendance,
  StudentAttendance,
} from '../common/interfaces/attendance.interface';
import { AttendanceService } from './attendance.service';
import { CourseService } from '../course/course.service';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
  styleUrls: ['./attendance.page.scss'],
})
export class AttendancePage implements OnInit {
  attendanceList: Array<Attendance>;
  progessValuePresent: number;
  progessValueAbsent: number;
  courseList: Array<Course>;
  selectedCourse: Attendance;
  selectCourse: any;
  attendances: Array<StudentAttendance>;
  currentDate: string;
  totalStudent: number;
  absentStudent: number;
  presentStudent: number;
  absent: number;
  present: number;
  segment: any;

  attendancesFilterList: Array<StudentAttendance>;

  date = new FormControl(new Date());
  constructor(
    private attendanceService: AttendanceService,
    private courseService: CourseService,
  ) {}

  ngOnInit() {
    this.attendanceList = this.attendanceService.attendanceList;
    this.courseList = [];

    this.selectCourse = { program: '', course_name: '' } as any;
    this.selectedCourse = {} as Attendance;
    this.attendances = [];
    this.attendancesFilterList = [];
    this.present = 0;
    this.absent = 0;
    this.absentStudent = 0;
    this.presentStudent = 0;
    this.segment = 'All';

    if (this.courseService.selectedCourse.value.program !== '') {
      this.selectCourse.program = this.courseService.selectedCourse.value.program;
      this.selectCourse.course_name = this.courseService.selectedCourse.value.course_name;
    }

    this.getCourseList();
  }

  compareObjects(o1: any, o2: any): boolean {
    return o1.program == o2.program && o1.course_name == o2.course_name;
  }

  getCourseList() {
    this.courseService.getCourseList().subscribe({
      next: res => {
        this.courseList = res;
        if (this.courseService.selectedCourse.value.program === '') {
          this.selectCourse =
            this.courseList && this.courseList.length
              ? this.courseList[0]
              : { program: '', course_name: '' };
        }
        this.getAttendance();
      },
    });
  }

  getAttendance() {
    this.attendanceService
      .getAttendance(this.selectCourse.program, this.selectCourse.course_name)
      .subscribe({
        next: res => {
          this.attendances = [];
          this.currentDate = this.getCurrentDate();
          this.selectedCourse = res;

          this.courseList.forEach(course => {
            this.selectedCourse.attendancesList.forEach(attendnace => {
              if (
                course.course_name === this.selectedCourse.course_name &&
                course.program === this.selectedCourse.program &&
                attendnace.date === this.currentDate
              ) {
                this.attendances.push(attendnace);
              }
            });
          });
          this.attendanceProgression();
          this.segmentChanged();
          this.studentCount();
        },
      });
  }

  attendanceProgression() {
    this.present = 0;
    this.absent = 0;
    this.attendances.forEach(attendance => {
      if (attendance.remarks === 'Present') {
        this.present++;
      } else {
        this.absent++;
      }
    });
    if (this.attendances.length !== 0) {
      this.progessValuePresent = this.present / this.attendances.length;

      this.progessValueAbsent = this.absent / this.attendances.length;
    } else {
      this.progessValuePresent = 0;
      this.progessValueAbsent = 1;
    }
  }

  getCurrentDate() {
    const newDate: Date = this.date.value;
    const currentDate = [
      newDate.getDate(),
      newDate.getMonth() + 1,
      newDate.getFullYear(),
    ].join('/');

    return currentDate;
  }

  studentCount() {
    this.totalStudent = this.attendances.length;

    this.absentStudent = this.absent;

    this.presentStudent = this.present;
  }

  segmentChanged() {
    this.attendancesFilterList = [];

    this.attendances.filter(item => {
      if (item.remarks === this.segment || this.segment === 'All') {
        this.attendancesFilterList.push(item);
        return item;
      }
    });
  }
}
