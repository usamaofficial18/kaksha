import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddDraftPageRoutingModule } from './add-draft-routing.module';

import { AddDraftPage } from './add-draft.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, AddDraftPageRoutingModule],
  declarations: [AddDraftPage],
})
export class AddDraftPageModule {}
