import { Injectable, BadRequestException } from '@nestjs/common';
import { ProgramWebhookDto } from '../../entity/program/program-webhook-dto';
import { ProgramService } from '../../entity/program/program.service';
import { Program } from '../../entity/program/program.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { PROGRAM_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class ProgramWebhookAggregateService {
  constructor(private readonly programService: ProgramService) {}

  programCreated(programPayload: ProgramWebhookDto) {
    return from(
      this.programService.findOne({
        name: programPayload.name,
      }),
    ).pipe(
      switchMap(program => {
        if (program) {
          return throwError(new BadRequestException(PROGRAM_ALREADY_EXISTS));
        }
        const provider = this.mapProgram(programPayload);
        return from(this.programService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapProgram(programPayload: ProgramWebhookDto) {
    const program = new Program();
    Object.assign(program, programPayload);
    program.uuid = uuidv4();
    program.isSynced = true;
    return program;
  }

  programDeleted(programPayload: ProgramWebhookDto) {
    this.programService.deleteOne({ name: programPayload.name });
  }

  programUpdated(programPayload: ProgramWebhookDto) {
    return from(
      this.programService.findOne({ name: programPayload.name }),
    ).pipe(
      switchMap(program => {
        if (!program) {
          return this.programCreated(programPayload);
        }
        program.isSynced = true;
        return from(
          this.programService.updateOne(
            { name: program.name },
            { $set: programPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
