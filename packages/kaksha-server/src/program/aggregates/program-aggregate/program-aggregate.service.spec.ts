import { Test, TestingModule } from '@nestjs/testing';
import { ProgramAggregateService } from './program-aggregate.service';
import { ProgramService } from '../../../program/entity/program/program.service';
describe('ProgramAggregateService', () => {
  let service: ProgramAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProgramAggregateService,
        {
          provide: ProgramService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ProgramAggregateService>(ProgramAggregateService);
  });
  ProgramAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
