import {
  IsOptional,
  IsString,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
export class ProgramDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  program_name: string;

  @IsOptional()
  @IsString()
  department: string;

  @IsOptional()
  @IsNumber()
  is_published: number;

  @IsOptional()
  @IsNumber()
  allow_self_enroll: number;

  @IsOptional()
  @IsNumber()
  is_featured: number;

  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => ProgramCourseDto)
  courses: ProgramCourseDto[];
}
export class ProgramCourseDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  course: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @IsOptional()
  @IsString()
  course_name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsNumber()
  required: number;
}
