import { IEvent } from '@nestjs/cqrs';
import { Program } from '../../entity/program/program.entity';

export class ProgramUpdatedEvent implements IEvent {
  constructor(public updatePayload: Program) {}
}
