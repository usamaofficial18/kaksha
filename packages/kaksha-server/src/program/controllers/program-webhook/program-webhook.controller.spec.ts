import { Test, TestingModule } from '@nestjs/testing';
import { ProgramWebhookController } from './program-webhook.controller';
import { ProgramWebhookAggregateService } from '../../aggregates/program-webhook-aggregate/program-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('ProgramWebhook Controller', () => {
  let controller: ProgramWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: ProgramWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [ProgramWebhookController],
    }).compile();

    controller = module.get<ProgramWebhookController>(ProgramWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
