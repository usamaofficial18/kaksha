import { ICommand } from '@nestjs/cqrs';
import { ProgramDto } from '../../entity/program/program-dto';

export class AddProgramCommand implements ICommand {
  constructor(
    public programPayload: ProgramDto,
    public readonly clientHttpRequest: any,
  ) {}
}
