import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TeacherAddedEvent } from './teacher-added.event';
import { TeacherService } from '../../entity/teacher/teacher.service';

@EventsHandler(TeacherAddedEvent)
export class TeacherAddedEventHandler
  implements IEventHandler<TeacherAddedEvent> {
  constructor(private readonly teacherService: TeacherService) {}
  async handle(event: TeacherAddedEvent) {
    const { teacher: teacher } = event;
    await this.teacherService.create(teacher);
  }
}
