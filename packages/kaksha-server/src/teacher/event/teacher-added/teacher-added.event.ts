import { IEvent } from '@nestjs/cqrs';
import { Teacher } from '../../entity/teacher/teacher.entity';

export class TeacherAddedEvent implements IEvent {
  constructor(public teacher: Teacher, public clientHttpRequest: any) {}
}
