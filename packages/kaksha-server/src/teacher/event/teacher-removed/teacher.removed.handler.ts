import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TeacherService } from '../../entity/teacher/teacher.service';
import { TeacherRemovedEvent } from './teacher-removed.event';

@EventsHandler(TeacherRemovedEvent)
export class TeacherRemovedEventHandler
  implements IEventHandler<TeacherRemovedEvent> {
  constructor(private readonly teacherService: TeacherService) {}
  async handle(event: TeacherRemovedEvent) {
    const { teacher: teacher } = event;
    await this.teacherService.deleteOne({ uuid: teacher.uuid });
  }
}
