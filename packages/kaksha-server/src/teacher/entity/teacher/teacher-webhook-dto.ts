import {
  IsString,
  IsOptional,
  ValidateNested,
  IsNumber,
  IsNotEmpty,
} from 'class-validator';
import { Type } from 'class-transformer';

export class TeacherWebhookDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  instructor_name: string;

  @IsString()
  @IsOptional()
  employee: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => TeacherLogWebhookDto)
  instructor_log: TeacherLogWebhookDto[];
}
export class TeacherLogWebhookDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  academic_year: string;

  @IsString()
  @IsOptional()
  academic_term: string;

  @IsString()
  @IsOptional()
  program: string;

  @IsString()
  @IsOptional()
  course: string;

  @IsString()
  @IsOptional()
  doctype: string;
}
