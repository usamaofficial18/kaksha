import {
  Injectable,
  BadRequestException,
  HttpService,
  NotFoundException,
} from '@nestjs/common';
import { TeacherWebhookDto } from '../../../teacher/entity/teacher/teacher-webhook-dto';
import { from, throwError, of, forkJoin } from 'rxjs';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { switchMap, map, catchError } from 'rxjs/operators';
import { Teacher } from '../../../teacher/entity/teacher/teacher.entity';
import * as uuidv4 from 'uuid/v4';
import { TEACHER_ALREADY_EXISTS } from '../../../constants/messages';
import { FRAPPE_API_GET_EMPLOYEE_INFO_ENDPOINT } from '../../../constants/routes';
import {
  BEARER_HEADER_VALUE_PREFIX,
  AUTHORIZATION,
} from '../../../constants/app-strings';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

@Injectable()
export class TeacherWebhookAggregateService {
  constructor(
    private readonly teacherService: TeacherService,
    private readonly clientToken: ClientTokenManagerService,
    private readonly http: HttpService,
    private readonly settings: SettingsService,
  ) {}

  createdTeacher(teacherPayload: TeacherWebhookDto) {
    return from(
      this.teacherService.findOne({ name: teacherPayload.name }),
    ).pipe(
      switchMap(teacher => {
        if (teacher) {
          return throwError(new BadRequestException(TEACHER_ALREADY_EXISTS));
        }
        const provider = this.mapTeacher(teacherPayload);
        return this.getUserDetails(teacherPayload.employee).pipe(
          switchMap(employee => {
            provider.instructor_email = employee.personal_email;
            return from(this.teacherService.create(provider)).pipe(
              switchMap(() => {
                return of({});
              }),
            );
          }),
        );
      }),
    );
  }
  mapTeacher(teacherPayload) {
    const teacher = new Teacher();
    Object.assign(teacher, teacherPayload);
    teacher.isSynced = true;
    teacher.uuid = uuidv4();
    return teacher;
  }

  deletedTeacher(teacherPayload: TeacherWebhookDto) {
    this.teacherService.deleteOne({ name: teacherPayload.name });
  }
  updatedTeacher(teacherPayload: TeacherWebhookDto) {
    return from(
      this.teacherService.findOne({
        name: teacherPayload.name,
      }),
    ).pipe(
      switchMap(teacher => {
        if (!teacher) {
          return this.createdTeacher(teacherPayload);
        }
        teacher.isSynced = true;
        return from(
          this.teacherService.updateOne(
            { name: teacher.name },
            { $set: teacherPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  getUserDetails(employee: string) {
    return forkJoin({
      token: this.clientToken.getClientToken(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ token, settings }) => {
        return this.http
          .get(
            settings.authServerURL +
              FRAPPE_API_GET_EMPLOYEE_INFO_ENDPOINT +
              employee,
            {
              headers: {
                [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + token.accessToken,
              },
            },
          )
          .pipe(map(res => res.data.data));
      }),
      catchError(err => throwError(new NotFoundException())),
    );
  }
}
