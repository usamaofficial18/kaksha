import { Test, TestingModule } from '@nestjs/testing';
import { TeacherWebhookController } from './teacher-webhook.controller';
import { TeacherWebhookAggregateService } from '../../../teacher/aggregates/teacher-webhook-aggregate/teacher-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('TeacherWebhook Controller', () => {
  let controller: TeacherWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TeacherWebhookController],
      providers: [
        {
          provide: TeacherWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<TeacherWebhookController>(TeacherWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
