import { Module, HttpModule } from '@nestjs/common';
import { ArticleAggregatesManager } from './aggregates';
import { ArticleEntitiesModule } from './entity/entity.module';
import { ArticleQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { ArticleCommandManager } from './command';
import { ArticleEventManager } from './event';
import { ArticleController } from './controllers/article/article.controller';
import { ArticlePoliciesService } from './policies/article-policies/article-policies.service';
import { ArticleWebhookController } from './controllers/article-webhook/article-webhook.controller';
import { TeacherModule } from '../teacher/teacher.module';
import { TopicModule } from '../topic/topic.module';
import { TopicEntitiesModule } from '../topic/entity/entity.module';
import { CourseEntitiesModule } from '../course/entity/entity.module';
import { TeacherEntitiesModule } from '../teacher/entity/entity.module';

@Module({
  imports: [
    TopicEntitiesModule,
    CourseEntitiesModule,
    TeacherEntitiesModule,
    ArticleEntitiesModule,
    CqrsModule,
    HttpModule,
    TeacherModule,
    TopicModule,
  ],
  controllers: [ArticleController, ArticleWebhookController],
  providers: [
    ...ArticleAggregatesManager,
    ...ArticleQueryManager,
    ...ArticleEventManager,
    ...ArticleCommandManager,
    ArticlePoliciesService,
  ],
  exports: [ArticleEntitiesModule],
})
export class ArticleModule {}
