import { IQuery } from '@nestjs/cqrs';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';

export class RetrieveArticleListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
    public clientHttpRequest: any,
    public getArticleList: UpdateTopicStatusDto,
  ) {}
}
