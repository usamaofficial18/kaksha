import { IsOptional, IsString, IsNumber } from 'class-validator';
export class UpdateArticleDto {
  @IsOptional()
  uuid: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  author: string;

  @IsOptional()
  @IsString()
  content: string;

  @IsString()
  @IsOptional()
  publish_date: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @IsOptional()
  @IsString()
  topic_name: string;
}
