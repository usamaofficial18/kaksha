import { Test, TestingModule } from '@nestjs/testing';
import { ArticlePoliciesService } from './article-policies.service';

describe('ArticlePoliciesService', () => {
  let service: ArticlePoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ArticlePoliciesService],
    }).compile();

    service = module.get<ArticlePoliciesService>(ArticlePoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
