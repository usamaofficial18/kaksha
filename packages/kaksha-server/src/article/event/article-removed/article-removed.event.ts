import { IEvent } from '@nestjs/cqrs';
import { Article } from '../../entity/article/article.entity';

export class ArticleRemovedEvent implements IEvent {
  constructor(public article: Article) {}
}
