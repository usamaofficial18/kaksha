import { Test, TestingModule } from '@nestjs/testing';
import { ArticleWebhookController } from './article-webhook.controller';
import { ArticleWebhookAggregateService } from '../../../article/aggregates/article-webhook-aggregate/article-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('ArticleWebhook Controller', () => {
  let controller: ArticleWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: ArticleWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [ArticleWebhookController],
    }).compile();

    controller = module.get<ArticleWebhookController>(ArticleWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
