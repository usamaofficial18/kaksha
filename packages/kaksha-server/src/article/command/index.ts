import { AddArticleCommandHandler } from './add-article/add-article.handler';
import { RemoveArticleCommandHandler } from './remove-article/remove-article.handler';
import { UpdateArticleCommandHandler } from './update-article/update-article.handler';
import { AddNewArticleCommandHandler } from './add-new-article/add-new-article.handler';
import { UpdateSingleArticleCommandHandler } from './update-single-article/update-single-article.handler';

export const ArticleCommandManager = [
  AddArticleCommandHandler,
  RemoveArticleCommandHandler,
  UpdateArticleCommandHandler,
  AddNewArticleCommandHandler,
  UpdateSingleArticleCommandHandler,
];
