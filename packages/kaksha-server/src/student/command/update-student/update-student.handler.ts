import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateStudentCommand } from './update-student.command';
import { StudentAggregateService } from '../../aggregates/student-aggregate/student-aggregate.service';

@CommandHandler(UpdateStudentCommand)
export class UpdateStudentCommandHandler
  implements ICommandHandler<UpdateStudentCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StudentAggregateService,
  ) {}

  async execute(command: UpdateStudentCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.update(updatePayload);
    aggregate.commit();
  }
}
