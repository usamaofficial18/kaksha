import { Test, TestingModule } from '@nestjs/testing';
import { StudentPoliciesService } from './student-policies.service';

describe('StudentcqrsPoliciesService', () => {
  let service: StudentPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StudentPoliciesService],
    }).compile();

    service = module.get<StudentPoliciesService>(StudentPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
