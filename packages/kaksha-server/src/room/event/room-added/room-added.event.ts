import { IEvent } from '@nestjs/cqrs';
import { Room } from '../../entity/room/room.entity';

export class RoomAddedEvent implements IEvent {
  constructor(public room: Room, public clientHttpRequest: any) {}
}
