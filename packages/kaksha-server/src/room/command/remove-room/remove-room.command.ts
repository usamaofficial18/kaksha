import { ICommand } from '@nestjs/cqrs';

export class RemoveRoomCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
