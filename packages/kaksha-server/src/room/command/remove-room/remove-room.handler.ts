import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveRoomCommand } from './remove-room.command';
import { RoomAggregateService } from '../../aggregates/room-aggregate/room-aggregate.service';

@CommandHandler(RemoveRoomCommand)
export class RemoveRoomCommandHandler
  implements ICommandHandler<RemoveRoomCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: RoomAggregateService,
  ) {}
  async execute(command: RemoveRoomCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
