import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { QuizDto } from '../../entity/quiz/quiz-dto';
import { AddQuizCommand } from '../../command/add-quiz/add-quiz.command';
import { RemoveQuizCommand } from '../../command/remove-quiz/remove-quiz.command';
import { UpdateQuizCommand } from '../../command/update-quiz/update-quiz.command';
import { RetrieveQuizQuery } from '../../query/get-quiz/retrieve-quiz.query';
import { RetrieveQuizListQuery } from '../../query/list-quiz/retrieve-quiz-list.query';
import { UpdateQuizDto } from '../../entity/quiz/update-quiz-dto';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';

@Controller('quiz')
export class QuizController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  createQuiz(@Body() quizPayload: QuizDto, @Req() req) {
    return this.commandBus.execute(new AddQuizCommand(quizPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveQuizCommand(uuid));
  }

  @Get('v1/get')
  @UseGuards(TokenGuard)
  async getQuiz(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Query() getQuizPayload: UpdateTopicStatusDto,
    @Req() req,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return await this.queryBus.execute(
      new RetrieveQuizQuery(offset, limit, sort, search, getQuizPayload, req),
    );
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getQuizList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveQuizListQuery(offset, limit, sort, search, clientHttpRequest),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateQuiz(@Body() updatePayload: UpdateQuizDto) {
    return this.commandBus.execute(new UpdateQuizCommand(updatePayload));
  }
}
