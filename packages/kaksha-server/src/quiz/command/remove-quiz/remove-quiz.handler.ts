import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveQuizCommand } from './remove-quiz.command';
import { QuizAggregateService } from '../../aggregates/quiz-aggregate/quiz-aggregate.service';

@CommandHandler(RemoveQuizCommand)
export class RemoveQuizCommandHandler
  implements ICommandHandler<RemoveQuizCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: QuizAggregateService,
  ) {}
  async execute(command: RemoveQuizCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
