import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateQuizCommand } from './update-quiz.command';
import { QuizAggregateService } from '../../aggregates/quiz-aggregate/quiz-aggregate.service';

@CommandHandler(UpdateQuizCommand)
export class UpdateQuizCommandHandler
  implements ICommandHandler<UpdateQuizCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuizAggregateService,
  ) {}

  async execute(command: UpdateQuizCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.update(updatePayload);
    aggregate.commit();
  }
}
