import { Injectable, BadRequestException } from '@nestjs/common';
import { QuizWebhookDto } from '../../entity/quiz/quiz-webhook-dto';
import { QuizService } from '../../../quiz/entity/quiz/quiz.service';
import { Quiz } from '../../../quiz/entity/quiz/quiz.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { QUIZ_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class QuizWebhookAggregateService {
  constructor(private readonly quizService: QuizService) {}

  quizCreated(quizPayload: QuizWebhookDto) {
    return from(
      this.quizService.findOne({
        name: quizPayload.name,
      }),
    ).pipe(
      switchMap(quiz => {
        if (quiz) {
          return throwError(new BadRequestException(QUIZ_ALREADY_EXISTS));
        }
        const provider = this.mapQuiz(quizPayload);
        return from(this.quizService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapQuiz(quizPayload: QuizWebhookDto) {
    const quiz = new Quiz();
    Object.assign(quiz, quizPayload);
    quiz.isSynced = true;
    quiz.uuid = uuidv4();
    return quiz;
  }

  quizDeleted(quizPayload: QuizWebhookDto) {
    this.quizService.deleteOne({ name: quizPayload.name });
  }

  quizUpdated(quizPayload: QuizWebhookDto) {
    return from(this.quizService.findOne({ name: quizPayload.name })).pipe(
      switchMap(quiz => {
        if (!quiz) {
          return this.quizCreated(quizPayload);
        }
        quiz.isSynced = true;
        return from(
          this.quizService.updateOne(
            { name: quiz.name },
            { $set: quizPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
