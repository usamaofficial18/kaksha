import { Module, HttpModule } from '@nestjs/common';
import { AttendanceAggregatesManager } from './aggregates';
import { AttendanceEntitiesModule } from './entity/entity.module';
import { AttendanceQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { AttendanceCommandManager } from './command';
import { AttendanceEventManager } from './event';
import { AttendanceController } from './controllers/attendance/attendance.controller';
import { AttendancePoliciesService } from './policies/attendance-policies/attendance-policies.service';
import { AttendanceWebhookController } from '../attendance/controllers/attendance-webhook/attendance-webhook.controller';
import { CourseScheduleEntitiesModule } from '../course-schedule/entity/entity.module';

@Module({
  imports: [
    AttendanceEntitiesModule,
    CourseScheduleEntitiesModule,
    CqrsModule,
    HttpModule,
  ],
  controllers: [AttendanceController, AttendanceWebhookController],
  providers: [
    ...AttendanceAggregatesManager,
    ...AttendanceQueryManager,
    ...AttendanceEventManager,
    ...AttendanceCommandManager,
    AttendancePoliciesService,
  ],
  exports: [AttendanceEntitiesModule],
})
export class AttendanceModule {}
