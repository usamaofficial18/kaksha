import { AttendanceAggregateService } from './attendance-aggregate/attendance-aggregate.service';
import { AttendanceWebhookAggregateService } from './attendance-webhook-aggregate/attendance-webhook-aggregate.service';

export const AttendanceAggregatesManager = [
  AttendanceAggregateService,
  AttendanceWebhookAggregateService,
];
