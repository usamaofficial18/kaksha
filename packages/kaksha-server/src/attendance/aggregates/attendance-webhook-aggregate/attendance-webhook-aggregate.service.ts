import { Injectable, BadRequestException } from '@nestjs/common';
import { AttendanceWebhookDto } from '../../entity/attendance/attendance-webhook-dto';
import { AttendanceService } from '../../../attendance/entity/attendance/attendance.service';
import { Attendance } from '../../../attendance/entity/attendance/attendance.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ATTENDANCE_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class AttendanceWebhookAggregateService {
  constructor(private readonly attendanceService: AttendanceService) {}

  attendanceCreated(attendancePayload: AttendanceWebhookDto) {
    return from(
      this.attendanceService.findOne({
        name: attendancePayload.name,
      }),
    ).pipe(
      switchMap(attendance => {
        if (attendance) {
          return throwError(new BadRequestException(ATTENDANCE_ALREADY_EXISTS));
        }
        const provider = this.mapAttendance(attendancePayload);
        return from(this.attendanceService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapAttendance(attendancePayload: AttendanceWebhookDto) {
    const attendance = new Attendance();
    Object.assign(attendance, attendancePayload);
    attendance.uuid = uuidv4();
    attendance.isSynced = true;
    return attendance;
  }

  attendanceDeleted(attendancePayload: AttendanceWebhookDto) {
    this.attendanceService.deleteOne({ name: attendancePayload.name });
  }

  attendanceUpdated(attendancePayload: AttendanceWebhookDto) {
    return from(
      this.attendanceService.findOne({ name: attendancePayload.name }),
    ).pipe(
      switchMap(attendance => {
        if (!attendance) {
          return this.attendanceCreated(attendancePayload);
        }
        attendance.isSynced = true;

        return from(
          this.attendanceService.updateOne(
            { name: attendance.name },
            { $set: attendancePayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
