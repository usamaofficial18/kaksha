import { IEvent } from '@nestjs/cqrs';
import { Attendance } from '../../entity/attendance/attendance.entity';

export class AttendanceRemovedEvent implements IEvent {
  constructor(public attendance: Attendance) {}
}
