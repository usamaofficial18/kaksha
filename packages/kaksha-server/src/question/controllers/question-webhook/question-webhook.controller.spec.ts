import { Test, TestingModule } from '@nestjs/testing';
import { QuestionWebhookController } from './question-webhook.controller';
import { QuestionWebhookAggregateService } from '../../aggregates/question-webhook-aggregate/question-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('QuestionWebhook Controller', () => {
  let controller: QuestionWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: QuestionWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [QuestionWebhookController],
    }).compile();

    controller = module.get<QuestionWebhookController>(
      QuestionWebhookController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
