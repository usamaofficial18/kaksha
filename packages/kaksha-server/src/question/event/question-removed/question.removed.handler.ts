import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuestionService } from '../../entity/question/question.service';
import { QuestionRemovedEvent } from './question-removed.event';

@EventsHandler(QuestionRemovedEvent)
export class QuestionRemovedCommandHandler
  implements IEventHandler<QuestionRemovedEvent> {
  constructor(private readonly questionService: QuestionService) {}
  async handle(event: QuestionRemovedEvent) {
    const { question } = event;
    await this.questionService.deleteOne({ uuid: question.uuid });
  }
}
