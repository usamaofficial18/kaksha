import { Test, TestingModule } from '@nestjs/testing';
import { QuestionAggregateService } from './question-aggregate.service';
import { QuestionService } from '../../entity/question/question.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { HttpService } from '@nestjs/common';
import { QuizWebhookAggregateService } from '../../../quiz/aggregates/quiz-webhook-aggregate/quiz-webhook-aggregate.service';

describe('QuestionAggregateService', () => {
  let service: QuestionAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuestionAggregateService,
        {
          provide: QuestionService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: QuizWebhookAggregateService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuestionAggregateService>(QuestionAggregateService);
  });
  QuestionAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
