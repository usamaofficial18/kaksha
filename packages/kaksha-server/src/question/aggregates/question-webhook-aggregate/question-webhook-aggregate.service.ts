import { Injectable, BadRequestException } from '@nestjs/common';
import { QuestionWebhookDto } from '../../entity/question/question-webhook-dto';
import { QuestionService } from '../../entity/question/question.service';
import { Question } from '../../entity/question/question.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { QUESTION_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class QuestionWebhookAggregateService {
  constructor(private readonly questionService: QuestionService) {}

  questionCreated(questionPayload: QuestionWebhookDto) {
    return from(
      this.questionService.findOne({
        name: questionPayload.name,
      }),
    ).pipe(
      switchMap(question => {
        if (question) {
          return throwError(new BadRequestException(QUESTION_ALREADY_EXISTS));
        }
        const provider = this.mapQuestion(questionPayload);
        return from(this.questionService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapQuestion(questionPayload: QuestionWebhookDto) {
    const question = new Question();
    Object.assign(question, questionPayload);
    question.isSynced = true;
    question.uuid = uuidv4();
    return question;
  }

  questionDeleted(questionPayload: QuestionWebhookDto) {
    this.questionService.deleteOne({ name: questionPayload.name });
  }

  questionUpdated(questionPayload: QuestionWebhookDto) {
    return from(
      this.questionService.findOne({ name: questionPayload.name }),
    ).pipe(
      switchMap(question => {
        if (!question) {
          return this.questionCreated(questionPayload);
        }
        question.isSynced = true;
        return from(
          this.questionService.updateOne(
            { name: question.name },
            { $set: questionPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
