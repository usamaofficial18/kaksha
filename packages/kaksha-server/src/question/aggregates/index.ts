import { QuestionAggregateService } from './question-aggregate/question-aggregate.service';
import { QuestionWebhookAggregateService } from './question-webhook-aggregate/question-webhook-aggregate.service';

export const QuestionAggregatesManager = [
  QuestionAggregateService,
  QuestionWebhookAggregateService,
];
