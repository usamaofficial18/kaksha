import { RetrieveQuestionQueryHandler } from './get-question/retrieve-question.handler';
import { RetrieveQuestionListQueryHandler } from './list-question/retrieve-question-list.handler';

export const QuestionQueryManager = [
  RetrieveQuestionQueryHandler,
  RetrieveQuestionListQueryHandler,
];
