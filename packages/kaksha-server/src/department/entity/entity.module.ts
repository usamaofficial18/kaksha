import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Department } from './department/department.entity';
import { DepartmentService } from './department/department.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Department]), CqrsModule],
  providers: [DepartmentService],
  exports: [DepartmentService],
})
export class DepartmentEntitiesModule {}
