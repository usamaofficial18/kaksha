import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Department extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  department_name: string;

  @Column()
  parent_department: string;

  @Column()
  company: string;

  @Column()
  is_group: number;

  @Column()
  disabled: number;

  @Column()
  lft: number;

  @Column()
  rgt: number;

  @Column()
  old_parent: string;

  @Column()
  doctype: string;

  @Column()
  leave_approvers: Approvers[];

  @Column()
  expense_approvers: Approvers[];

  @Column()
  isSynced: boolean;
}

export class Approvers {
  name: string;
  docstatus: number;
  approver: string;
  doctype: string;
}
