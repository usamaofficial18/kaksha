import { RetrieveDepartmentQueryHandler } from './get-department/retrieve-department.handler';
import { RetrieveDepartmentListQueryHandler } from './list-department/retrieve-department-list.handler';

export const DepartmentQueryManager = [
  RetrieveDepartmentQueryHandler,
  RetrieveDepartmentListQueryHandler,
];
