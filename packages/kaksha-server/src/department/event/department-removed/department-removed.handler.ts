import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DepartmentService } from '../../entity/department/department.service';
import { DepartmentRemovedEvent } from './department-removed.event';

@EventsHandler(DepartmentRemovedEvent)
export class DepartmentRemovedCommandHandler
  implements IEventHandler<DepartmentRemovedEvent> {
  constructor(private readonly departmentService: DepartmentService) {}
  async handle(event: DepartmentRemovedEvent) {
    const { department } = event;
    await this.departmentService.deleteOne({ uuid: department.uuid });
  }
}
