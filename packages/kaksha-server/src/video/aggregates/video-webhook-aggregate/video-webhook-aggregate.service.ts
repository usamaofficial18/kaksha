import { Injectable, BadRequestException } from '@nestjs/common';
import { VideoWebhookDto } from '../../entity/video/video-webhook-dto';
import { VideoService } from '../../../video/entity/video/video.service';
import { Video } from '../../../video/entity/video/video.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { VIDEO_ALREADY_EXISTS } from '../../../constants/messages';

import * as uuidv4 from 'uuid/v4';
@Injectable()
export class VideoWebhookAggregateService {
  constructor(private readonly videoService: VideoService) {}

  videoCreated(videoPayload: VideoWebhookDto) {
    return from(
      this.videoService.findOne({
        name: videoPayload.name,
      }),
    ).pipe(
      switchMap(video => {
        if (video) {
          return throwError(new BadRequestException(VIDEO_ALREADY_EXISTS));
        }
        const provider = this.mapVideo(videoPayload);
        return from(this.videoService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapVideo(videoPayload: VideoWebhookDto) {
    const video = new Video();
    Object.assign(video, videoPayload);
    video.isSynced = true;
    video.uuid = uuidv4();
    return video;
  }

  videoDeleted(videoPayload: VideoWebhookDto) {
    this.videoService.deleteOne({ name: videoPayload.name });
  }

  videoUpdated(videoPayload: VideoWebhookDto) {
    return from(this.videoService.findOne({ name: videoPayload.name })).pipe(
      switchMap(video => {
        if (!video) {
          return this.videoCreated(videoPayload);
        }
        video.isSynced = true;
        return from(
          this.videoService.updateOne(
            { name: video.name },
            { $set: videoPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
