import { IQuery } from '@nestjs/cqrs';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';

export class RetrieveVideoListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
    public getVideoPayload: GetCourseQueryDto,
    public clientHttpRequest: any,
  ) {}
}
