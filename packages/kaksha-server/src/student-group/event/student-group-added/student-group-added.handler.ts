import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StudentGroupAddedEvent } from './student-group-added.event';
import { StudentGroupService } from '../../entity/student-group/student-group.service';

@EventsHandler(StudentGroupAddedEvent)
export class StudentGroupAddedCommandHandler
  implements IEventHandler<StudentGroupAddedEvent> {
  constructor(private readonly studentGroupService: StudentGroupService) {}
  async handle(event: StudentGroupAddedEvent) {
    const { studentGroup } = event;
    await this.studentGroupService.create(studentGroup);
  }
}
