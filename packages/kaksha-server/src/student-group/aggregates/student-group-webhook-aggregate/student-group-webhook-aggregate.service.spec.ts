import { Test, TestingModule } from '@nestjs/testing';
import { StudentGroupWebhookAggregateService } from './student-group-webhook-aggregate.service';
import { StudentGroupService } from '../../../student-group/entity/student-group/student-group.service';

describe('StudentGroupWebhookAggregateService', () => {
  let service: StudentGroupWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StudentGroupWebhookAggregateService,
        {
          provide: StudentGroupService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StudentGroupWebhookAggregateService>(
      StudentGroupWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
