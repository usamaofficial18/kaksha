import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { CourseScheduleDto } from '../../entity/course-schedule/course-schedule-dto';
import { AddCourseScheduleCommand } from '../../command/add-course-schedule/add-course-schedule.command';
import { RemoveCourseScheduleCommand } from '../../command/remove-course-schedule/remove-course-schedule.command';
import { UpdateCourseScheduleCommand } from '../../command/update-course-schedule/update-course-schedule.command';
import { RetrieveCourseScheduleQuery } from '../../query/get-course-schedule/retrieve-course-schedule.query';
import { RetrieveCourseScheduleListQuery } from '../../query/list-course-schedule/retrieve-course-schedule-list.query';
import { UpdateCourseScheduleDto } from '../../entity/course-schedule/update-course-schedule-dto';

@Controller('course_schedule')
export class CourseScheduleController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() courseSchedulePayload: CourseScheduleDto, @Req() req) {
    return this.commandBus.execute(
      new AddCourseScheduleCommand(courseSchedulePayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveCourseScheduleCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(
      new RetrieveCourseScheduleQuery(uuid, req),
    );
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveCourseScheduleListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateCourseScheduleDto) {
    return this.commandBus.execute(
      new UpdateCourseScheduleCommand(updatePayload),
    );
  }
}
