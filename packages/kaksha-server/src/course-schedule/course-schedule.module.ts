import { Module, HttpModule } from '@nestjs/common';
import { CourseScheduleAggregatesManager } from './aggregates';
import { CourseScheduleEntitiesModule } from './entity/entity.module';
import { CourseScheduleQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { CourseScheduleCommandManager } from './command';
import { CourseScheduleEventManager } from './event';
import { CourseScheduleController } from './controllers/course-schedule/course-schedule.controller';
import { CourseSchedulePoliciesService } from './policies/course-schedule-policies/course-schedule-policies.service';
import { CourseScheduleWebhookController } from '../course-schedule/controllers/course-schedule-webhook/course-schedule-webhook.controller';

@Module({
  imports: [CourseScheduleEntitiesModule, CqrsModule, HttpModule],
  controllers: [CourseScheduleController, CourseScheduleWebhookController],
  providers: [
    ...CourseScheduleAggregatesManager,
    ...CourseScheduleQueryManager,
    ...CourseScheduleEventManager,
    ...CourseScheduleCommandManager,
    CourseSchedulePoliciesService,
  ],
  exports: [CourseScheduleEntitiesModule],
})
export class CourseScheduleModule {}
