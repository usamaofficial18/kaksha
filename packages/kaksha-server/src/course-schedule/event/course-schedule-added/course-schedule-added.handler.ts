import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseScheduleAddedEvent } from './course-schedule-added.event';
import { CourseScheduleService } from '../../entity/course-schedule/course-schedule.service';

@EventsHandler(CourseScheduleAddedEvent)
export class CourseScheduleAddedCommandHandler
  implements IEventHandler<CourseScheduleAddedEvent> {
  constructor(private readonly courseScheduleService: CourseScheduleService) {}
  async handle(event: CourseScheduleAddedEvent) {
    const { courseSchedule } = event;
    await this.courseScheduleService.create(courseSchedule);
  }
}
