import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveCourseScheduleListQuery } from './retrieve-course-schedule-list.query';
import { CourseScheduleAggregateService } from '../../aggregates/course-schedule-aggregate/course-schedule-aggregate.service';

@QueryHandler(RetrieveCourseScheduleListQuery)
export class RetrieveCourseScheduleListQueryHandler
  implements IQueryHandler<RetrieveCourseScheduleListQuery> {
  constructor(private readonly manager: CourseScheduleAggregateService) {}
  async execute(query: RetrieveCourseScheduleListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getCourseScheduleList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
