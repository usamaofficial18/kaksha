import { ICommand } from '@nestjs/cqrs';
import { CourseScheduleDto } from '../../entity/course-schedule/course-schedule-dto';

export class AddCourseScheduleCommand implements ICommand {
  constructor(
    public courseSchedulePayload: CourseScheduleDto,
    public readonly clientHttpRequest: any,
  ) {}
}
