import { ICommand } from '@nestjs/cqrs';

export class RemoveCourseCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
