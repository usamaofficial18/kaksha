import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveCourseCommand } from './remove-course.command';
import { CourseAggregateService } from '../../aggregates/course-aggregate/course-aggregate.service';

@CommandHandler(RemoveCourseCommand)
export class RemoveCourseCommandHandler
  implements ICommandHandler<RemoveCourseCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: CourseAggregateService,
  ) {}
  async execute(command: RemoveCourseCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
