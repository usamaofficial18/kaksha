import { IEvent } from '@nestjs/cqrs';
import { Course } from '../../entity/course/course.entity';

export class CourseUpdatedEvent implements IEvent {
  constructor(public updatePayload: Course) {}
}
