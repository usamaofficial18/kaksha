import { CourseAggregateService } from './course-aggregate/course-aggregate.service';
import { CourseWebhookAggregateService } from './course-webhook-aggregate/course-webhook-aggregate.service';

export const CourseAggregatesManager = [
  CourseAggregateService,
  CourseWebhookAggregateService,
];
