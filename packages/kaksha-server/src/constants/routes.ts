export const FRAPPE_API_GET_USER_INFO_ENDPOINT = '/api/resource/User/';
export const CLIENT_CALLBACK_ENDPOINT = '/home/callback';
export const FRAPPE_API_GET_OAUTH_BEARER_TOKEN_ENDPOINT =
  '/api/resource/OAuth%20Bearer%20Token/';
export const STUDENT_AFTER_INSERT_ENDPOINT = '/api/student/webhook/v1/create';
export const STUDENT_ON_UPDATE_ENDPOINT = '/api/student/webhook/v1/update';
export const STUDENT_ON_TRASH_ENDPOINT = '/api/student/webhook/v1/delete';
export const FRAPPE_API_GET_STUDENT_ENDPOINT = '/api/resource/Student';
export const TEACHER_AFTER_INSERT_ENDPOINT = '/api/teacher/webhook/v1/create';
export const TEACHER_ON_UPDATE_ENDPOINT = '/api/teacher/webhook/v1/update';
export const TEACHER_ON_TRASH_ENDPOINT = '/api/teacher/webhook/v1/delete';
export const FRAPPE_API_GET_TEACHER_ENDPOINT = '/api/resource/Instructor';
export const FRAPPE_API_GET_PROGRAM_ENDPOINT = '/api/resource/Program';
export const PROGRAM_AFTER_INSERT_ENDPOINT = '/api/program/webhook/v1/create';
export const PROGRAM_ON_UPDATE_ENDPOINT = '/api/program/webhook/v1/update';
export const PROGRAM_ON_TRASH_ENDPOINT = '/api/program/webhook/v1/delete';
export const COURSE_ON_AFTER_INSERT_ENDPOINT = '/api/course/webhook/v1/create';
export const COURSE_ON_UPDATE_ENDPOINT = '/api/course/webhook/v1/update';
export const COURSE_ON_TRASH_ENDPOINT = '/api/course/webhook/v1/delete';
export const FRAPPE_API_GET_COURSE_ENDPOINT = '/api/resource/Course/';
export const TOPIC_ON_AFTER_INSERT_ENDPOINT = '/api/topic/webhook/v1/create';
export const TOPIC_ON_UPDATE_ENDPOINT = '/api/topic/webhook/v1/update';
export const TOPIC_ON_TRASH_ENDPOINT = '/api/topic/webhook/v1/delete';
export const FRAPPE_API_GET_TOPIC_ENDPOINT = '/api/resource/Topic';
export const VIDEO_ON_AFTER_INSERT_ENDPOINT = '/api/video/webhook/v1/create';
export const VIDEO_ON_UPDATE_ENDPOINT = '/api/video/webhook/v1/update';
export const VIDEO_ON_TRASH_ENDPOINT = '/api/video/webhook/v1/delete';
export const FRAPPE_API_GET_VIDEO_ENDPOINT = '/api/resource/Video';
export const ARTICLE_ON_AFTER_INSERT_ENDPOINT =
  '/api/article/webhook/v1/create';
export const ARTICLE_ON_UPDATE_ENDPOINT = '/api/article/webhook/v1/update';
export const ARTICLE_ON_TRASH_ENDPOINT = '/api/article/webhook/v1/delete';
export const FRAPPE_API_GET_ARTICLE_ENDPOINT = '/api/resource/Article';
export const QUIZ_ON_AFTER_INSERT_ENDPOINT = '/api/quiz/webhook/v1/create';
export const QUIZ_ON_UPDATE_ENDPOINT = '/api/quiz/webhook/v1/update';
export const QUIZ_ON_TRASH_ENDPOINT = '/api/quiz/webhook/v1/delete';
export const FRAPPE_API_GET_QUIZ_ENDPOINT = '/api/resource/Quiz';
export const QUESTION_ON_AFTER_INSERT_ENDPOINT =
  '/api/question/webhook/v1/create';
export const QUESTION_ON_UPDATE_ENDPOINT = '/api/question/webhook/v1/update';
export const QUESTION_ON_TRASH_ENDPOINT = '/api/question/webhook/v1/delete';
export const FRAPPE_API_GET_QUESTION_ENDPOINT = '/api/resource/Question';
export const DEPARTMENT_ON_AFTER_INSERT_ENDPOINT =
  '/api/department/webhook/v1/create';
export const DEPARTMENT_ON_UPDATE_ENDPOINT =
  '/api/department/webhook/v1/update';
export const DEPARTMENT_ON_TRASH_ENDPOINT = '/api/department/webhook/v1/delete';
export const FRAPPE_API_GET_DEPARTMENT_ENDPOINT = '/api/resource/Department';
export const ATTENDANCE_ON_AFTER_INSERT_ENDPOINT =
  '/api/attendance/webhook/v1/create';
export const ATTENDANCE_ON_UPDATE_ENDPOINT =
  '/api/attendance/webhook/v1/update';
export const ATTENDANCE_ON_TRASH_ENDPOINT = '/api/attendance/webhook/v1/delete';
export const FRAPPE_API_GET_ATTENDANCE_ENDPOINT =
  '/api/resource/Student%20Attendance';
export const STUDENT_GROUP_ON_AFTER_INSERT_ENDPOINT =
  '/api/student_group/webhook/v1/create';
export const STUDENT_GROUP_ON_UPDATE_ENDPOINT =
  '/api/student_group/webhook/v1/update';
export const STUDENT_GROUP_ON_TRASH_ENDPOINT =
  '/api/student_group/webhook/v1/delete';
export const FRAPPE_API_GET_STUDENT_GROUP_ENDPOINT =
  '/api/resource/Student%20Group';
export const ROOM_ON_AFTER_INSERT_ENDPOINT = '/api/room/webhook/v1/create';
export const ROOM_ON_UPDATE_ENDPOINT = '/api/room/webhook/v1/update';
export const ROOM_ON_TRASH_ENDPOINT = '/api/room/webhook/v1/delete';
export const FRAPPE_API_GET_ROOM_ENDPOINT = '/api/resource/Room';
export const COURSE_SCHEDULE_ON_AFTER_INSERT_ENDPOINT =
  '/api/course_schedule/webhook/v1/create';
export const COURSE_SCHEDULE_ON_UPDATE_ENDPOINT =
  '/api/course_schedule/webhook/v1/update';
export const COURSE_SCHEDULE_ON_TRASH_ENDPOINT =
  '/api/course_schedule/webhook/v1/delete';
export const FRAPPE_API_GET_COURSE_SCHEDULE_ENDPOINT =
  '/api/resource/Course%20Schedule';
export const FRAPPE_API_GET_EMPLOYEE_INFO_ENDPOINT = '/api/resource/Employee/';
