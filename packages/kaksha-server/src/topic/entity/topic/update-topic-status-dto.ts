import { IsString, IsNotEmpty } from 'class-validator';

export class UpdateTopicStatusDto {
  @IsNotEmpty()
  @IsString()
  program_name: string;

  @IsNotEmpty()
  @IsString()
  course_name: string;

  @IsString()
  @IsNotEmpty()
  topic_name: string;
}
