import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Topic } from './topic/topic.entity';
import { TopicService } from './topic/topic.service';
import { CqrsModule } from '@nestjs/cqrs';
import { TopicWebhookAggregateService } from '../aggregates/topic-webhook-aggregate/topic-webhook-aggregate.service';

@Module({
  imports: [TypeOrmModule.forFeature([Topic]), CqrsModule],
  providers: [TopicService, TopicWebhookAggregateService],
  exports: [TopicService, TopicWebhookAggregateService],
})
export class TopicEntitiesModule {}
