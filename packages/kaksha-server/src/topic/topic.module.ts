import { Module, HttpModule } from '@nestjs/common';
import { TopicAggregatesManager } from './aggregates';
import { TopicEntitiesModule } from './entity/entity.module';
import { TopicQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { TopicCommandManager } from './command';
import { TopicEventManager } from './event';
import { TopicController } from './controllers/topic/topic.controller';
import { TopicPoliciesService } from './policies/topic-policies/topic-policies.service';
import { TopicWebhookController } from './controllers/topic-webhook/topic-webhook.controller';
// import { CourseModule } from '../course/course.module';
import { CourseEntitiesModule } from '../course/entity/entity.module';

@Module({
  imports: [TopicEntitiesModule, CourseEntitiesModule, CqrsModule, HttpModule],
  controllers: [TopicController, TopicWebhookController],
  providers: [
    ...TopicAggregatesManager,
    ...TopicQueryManager,
    ...TopicEventManager,
    ...TopicCommandManager,
    TopicPoliciesService,
  ],
  exports: [TopicEntitiesModule],
})
export class TopicModule {}
