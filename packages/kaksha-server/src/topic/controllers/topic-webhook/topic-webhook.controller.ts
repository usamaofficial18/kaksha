import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { TopicWebhookDto } from '../../entity/topic/topic-webhook-dto';
import { TopicWebhookAggregateService } from '../../aggregates/topic-webhook-aggregate/topic-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('topic')
export class TopicWebhookController {
  constructor(
    private readonly topicWebhookAggreagte: TopicWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  topicCreated(@Body() topicPayload: TopicWebhookDto) {
    return this.topicWebhookAggreagte.topicCreated(topicPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  topicUpdated(@Body() topicPayload: TopicWebhookDto) {
    return this.topicWebhookAggreagte.topicUpdated(topicPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  topicDeleted(@Body() topicPayload: TopicWebhookDto) {
    return this.topicWebhookAggreagte.topicDeleted(topicPayload);
  }
}
