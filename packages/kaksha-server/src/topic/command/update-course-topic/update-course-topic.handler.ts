import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateTopicStatusCommand } from './update-course-topic.command';
import { TopicAggregateService } from '../../../topic/aggregates/topic-aggregate/topic-aggregate.service';

@CommandHandler(UpdateTopicStatusCommand)
export class UpdateTopicsStatusCommandHandler
  implements ICommandHandler<UpdateTopicStatusCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TopicAggregateService,
  ) {}
  async execute(command: UpdateTopicStatusCommand) {
    const { updateTopicStatusPayload, req } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateTopicStatus(updateTopicStatusPayload, req);
    aggregate.commit();
  }
}
