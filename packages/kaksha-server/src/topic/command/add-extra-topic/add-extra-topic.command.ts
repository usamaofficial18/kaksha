import { ICommand } from '@nestjs/cqrs';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';

export class AddExtraTopicCommand implements ICommand {
  constructor(
    public readonly addTopicPayload: UpdateTopicStatusDto,
    public readonly clientHttpRequest: any,
  ) {}
}
